/*
 * Arduino.cpp
 *
 *  Created on: Mar 13, 2015
 *      Author: Brian
 */

#include <Arduino.h>

ExampleSubsystem* Arduino::examplesubsystem = NULL;

Arduino::Arduino(int baud):SerialPort(baud,Port::kOnboard),Timer() {
	messagesize=0; //including barker codes
	initialx=0;
	initialy = 0;
	initialheading = 0;
	barkerbegin[0] = 0;
	barkerbegin[1] = 0;
	barkerend = 0;
	x = 0;
	y = 0;
	heading = 0;
	Start();
	SetTimeout(.005);
	SetWriteBufferMode(WriteBufferMode::kFlushOnAccess);
	EnableTermination();
}

//Arduino::Arduino():SerialPort(57600),Timer(){}

Arduino::~Arduino() {
	// TODO Auto-generated destructor stub
}


	void Arduino::read(){   //not done, copied from out dated function
		int32_t bytesreceived = 0;

		bytesreceived = GetBytesReceived();

		if(bytesreceived>=messagesize)
		{
			bytesreceived = (bytesreceived / 20) * 20;
			char tempbuf[bytesreceived];
			Read(tempbuf,bytesreceived);
			int offset = 0;
			bool fail = 1;

			int beginloc = 0;

			for(offset = 0; offset < messagesize && (bytesreceived - offset) >= 20;offset++){ //search for barkercode
				if(tempbuf[bytesreceived - offset -1] == barkerend &&( tempbuf[bytesreceived - 21 - offset] == barkerbegin[0]||tempbuf[bytesreceived - 21 - offset] == barkerbegin[0])){
					fail = 0;
					beginloc = bytesreceived - 20 - offset;
					break;
				}
			}
			if(!fail){ 			//if the barkercode was found
				Timer::Reset();

				x = tempbuf[beginloc];
				x +=x*256+ tempbuf[beginloc+1];

				y = tempbuf[beginloc+2];
				y +=y*256+ tempbuf[beginloc+3];

				heading = tempbuf[beginloc+4];
				heading +=heading*256+ tempbuf[beginloc+5];

				y = tempbuf[beginloc+2];
				y +=y*256+ tempbuf[beginloc+3];

				//still need to add pitch and distances
			}
		}
	}


	void Arduino::setinitialrelative(int x, int y, int heading){
		Arduino::x -= initialx;
		Arduino::y -= initialy;

		initialx = x;
		initialy = y;

		initialheading = heading;

	}


	void Arduino::setbarkerbegin(char barker0,char barker1){
		barkerbegin[0] = barker0;
		barkerbegin[1] = barker1;
	}

	void Arduino::setbarkerend(char barker){
		barkerend = int(barker);
	}

	void Arduino::setmessagesize(int size){
		messagesize = size;
	}

	bool Arduino::update(int16_t data[3]){
		read();
		data[0] = x;
		data[1] = y;
		data[2] = heading;

		if(Get()<.25)
			return true;

		return false;
	}


