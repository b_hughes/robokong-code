/*
 * ArdGyro.cpp
 *
 *  Created on: Jun 16, 2015
 *      Author: Brian
 */

#include <ArdGyro.h>

ArdGyro::ArdGyro(int baud=57600):SerialPort(baud,Port::kUSB) {
	SetTimeout(.005);
	SetWriteBufferMode(WriteBufferMode::kFlushOnAccess);
	SetFlowControl(FlowControl::kFlowControl_RtsCts);
	EnableTermination();


}

//reads message in the form of Yaw,Pitch,Roll;
//may need to add endl char at end

bool ArdGyro::read(float &yaw1,float &pitch1,float &roll1){

	int size = GetBytesReceived();
	int messageSize = 3*4+3;

	uint32_t yaw,pitch,roll;

	if(size>=messageSize){
		size = size/messageSize*messageSize;
		char buf[size];

		Read(buf,size);

		if(buf[size-1]==';'){

			yaw = buf[size-5];
			yaw << 8;
			yaw += buf[size-4];
			yaw << 8;
			yaw += buf[size-3];
			yaw << 8;
			yaw += buf[size-2];

			yaw1=float(yaw);

			pitch = buf[size-10];
			pitch << 8;
			pitch += buf[size-9];
			pitch << 8;
			pitch += buf[size-8];
			pitch << 8;
			pitch += buf[size-7];

			pitch1 = float(pitch);

			roll = buf[size-12];
			roll << 8;
			roll += buf[size-13];
			roll << 8;
			roll += buf[size-14];
			roll << 8;
			roll += buf[size-15];

			roll1 = float(roll);

			return true;
		}

	}
	return false;
}
