#include "WPILib.h"
#include "Commands/Command.h"
#include "Commands/ExampleCommand.h"
#include "CommandBase.h"
#include "stdlib.h"
#include "string"
#include <iostream>
#include <stdio.h>


#define MOTORSOFF 0  //turns all motors off for testing when 1
#define DEBUG  0     //turns on debuging statements when 1
#define GYROON  1    //turns on the use of the gyro when 1



class Robot: public IterativeRobot
{
	float xpower;
	float ypower;
	float rotation;

	float data[3];
	bool dataflag = 0;
	int dropcount=0; //keeps track of the how many updates happen between getting gyro data

	double time = 0; //keeps track of time for running drive loop at 200hz

	RobotDrive *drive = new RobotDrive(2,3,1,0); //this needs to be checked with motor inputs
	DriverStation *ds;




private:
	Command *autonomousCommand;
	LiveWindow *lw;

	void RobotInit()
	{
		CommandBase::init();
		autonomousCommand = new ExampleCommand();
		lw = LiveWindow::GetInstance();
		ds = DriverStation::GetInstance();
		drive->SetExpiration(0.1);

		//these may need to be removed to get the robot driving correctly
		drive->SetInvertedMotor(RobotDrive::kFrontLeftMotor, true);
		drive->SetInvertedMotor(RobotDrive::kRearLeftMotor, true);

	}
	
	void DisabledPeriodic()
	{
		Scheduler::GetInstance()->Run();
	}

	void AutonomousInit()
	{
		if (autonomousCommand != NULL)
			autonomousCommand->Start();
	}

	void AutonomousPeriodic()
	{
		Scheduler::GetInstance()->Run();
	}

	void TeleopInit()
	{
		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to 
		// continue until interrupted by another command, remove
		// this line or comment it out.
		if (autonomousCommand != NULL)
			autonomousCommand->Cancel();
	}

	void TeleopPeriodic()
	{
		Scheduler::GetInstance()->Run();



		 if(Timer::GetFPGATimestamp() - time >= .005){
			 time = Timer::GetFPGATimestamp();


		xpower = CommandBase::oi->joystick1->GetRawAxis(0);
		ypower = CommandBase::oi->joystick1->GetRawAxis(1);
		rotation = CommandBase::oi->joystick1->GetRawAxis(5); //reads joystick positions

		dataflag = CommandBase::oi->ard->read(data[0],data[1],data[2]); //reads yaw pitch roll

		if(dataflag){
			dropcount=0;
		}
		else{
			dropcount++;
			char str[100];
			sprintf(str,"dropped %i \n", dropcount);
			if(dropcount>5)
				DriverStation::ReportError(str);  //reports an error on the driver station if more than 4 updates have passed without new gyro data
		}

#if  DEBUG
		system("cls");
		std::cout << xpower << " "<< ypower<< " "<< rotation <<" "<< data[0] << std::endl;
#endif

#if   MOTORSOFF
		drive->MecanumDrive_Cartesian(0,0,0,0);
#else

#if GYROON
		drive->MecanumDrive_Cartesian(xpower,ypower,rotation,data[0]);
#else
		drive->MecanumDrive_Cartesian(xpower,ypower,rotation,0);
#endif
#endif

	}
	}

	void TestPeriodic()
	{
		lw->Run();
	}
};

START_ROBOT_CLASS(Robot);

