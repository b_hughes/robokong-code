/*
 * ArdGyro.h
 *
 *  Created on: Jun 16, 2015
 *      Author: Brian
 */

#ifndef SRC_ARDGYRO_H_
#define SRC_ARDGYRO_H_

#include "OI.h"
#include "WPILib.h"
#include "Subsystems/ExampleSubsystem.h"

class ArdGyro: public SerialPort
{
public:
	ArdGyro(int);

	bool read(float&,float&,float&);


};

#endif /* SRC_ARDGYRO_H_ */
