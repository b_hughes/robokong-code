/*
 * Arduino.h
 *
 *  Created on: Mar 13, 2015
 *      Author: Brian
 */

#ifndef SRC_ARDUINO_H_
#define SRC_ARDUINO_H_

#include "OI.h"
#include "WPILib.h"
#include "Subsystems/ExampleSubsystem.h"


class Arduino: public SerialPort, public Timer
{
public:
	Arduino(int);
	Arduino();
	virtual ~Arduino();
	static void init(int);
	void read(); 				//designed specifically for frc 2015
	void setinitialrelative(int , int , int); //initial position of the robot
	void setbarkerbegin(char,char); //sets the barker begin codes (beginning of frame)
	void setbarkerend(char); //sets the end barker code (end of frame)
	void setmessagesize(int); //number of words in the message
	bool update(int16_t[3]); //get the newest values from the arduino if available

	static ExampleSubsystem *examplesubsystem;

private:
	int messagesize; //including barker codes
	int initialx;
	int initialy;
	int initialheading;
	char barkerbegin[2];
	char barkerend;
	int x;
	int y;
	int heading;

};

#endif /* SRC_ARDUINO_H_ */
