#ifndef OI_H
#define OI_H

#include "WPILib.h"
#include "SerialPort.h"
#include "Arduino.h"

class Arduino;

class OI
{
private:

public:
	OI();
	Joystick *joystick1;
	Joystick *joystick2;
	ArdGyro *ard;
};

#endif
